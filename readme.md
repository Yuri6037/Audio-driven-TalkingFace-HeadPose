# Audio-driven Talking Face Video Generation with Learning-based Personalized Head Pose

We provide PyTorch implementations for our arxiv paper "Audio-driven Talking Face Video Generation with Learning-based Personalized Head Pose"(http://arxiv.org/abs/2002.10137).

Note that this code is protected under patent. It is for research purposes only at your university (research institution) only. If you are interested in business purposes/for-profit use, please contact Prof.Liu (the corresponding author, email: liuyongjin@tsinghua.edu.cn).

We provide a demo video [here](https://cg.cs.tsinghua.edu.cn/people/~Yongjin/Yongjin.htm) (please search for "Talking Face" in this page and click the "demo video" button).

[Colab](https://colab.research.google.com/drive/1gqcqTSAGAyj48n0fmApvSPG_43BzKP37) (not updated)

## Our Proposed Framework

<img alt="pipeline" src='pipeline.jpg'>

## Prerequisites
- Linux (MacOS probably won't work)
- NVIDIA GPU
- Python 3
- MATLAB

## Getting Started

### Installation

#### NVIDIA
For this project will need CUDA 10.2 and CUDA 10.0. The reason why is because Tensorflow is a crappy lib unable to use CUDA 10.2 while other libs uses CUDA 10.2 instead of CUDA 10.0.
- You need to install at least NVIDIA driver 470.57.02 otherwise installing libnccl2 will cause destruction of NVIDIA
- You need to install CUDA 10.2 and CUDA 10.0, this won't work with CUDA 11
- You should install CUDNN otherwise Tensorflow being a crappy lib can't work with only CUDA. You do not need it but using it will improve performance. Unfortunatly NVIDIA are annoying and refuse to give these libs without an account. If you use System76 hardware you can install CUDNN from their repositories.
- Follow the following bash code in order to install NVIDIA's libnccl2 which is required by mxnet
```bash
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /"
sudo apt-get update
sudo apt install libnccl2
```
You may need to change that bash code if you're using a version of Ubuntu greater than 18.04. Other distributions have not been tested.

#### MATLAB
You need to install [MATLAB](https://www.mathworks.com/products/matlab.html) from MathWorks.

Once you have installed MATLAB make sure you have also installed the "image processing toolbox" from MATLAB GUI otherwise bwboundaries won't work.
In order to do this you may need to `chown your-licensed-user:your-licensed-user -R /usr/local/MATLAB` or MATLAB might complain that it does not have permission. 

#### Other
- You need to install BAZEL build tool in order to compile the mesh renderer kernels
- You should not trust requirements.txt as it may segfault pip (no idea why by the way)
- You need to install python3 and pip3 on your distribution, then you need to call `sudo pip3 install --upgrade pip` to upgrade PIP install (this is needed for mxnet)
- Install the following pip packages:
```bash
pip install torch torchvision torchaudio
pip install tensorflow==1.15
pip install tensorflow-gpu==1.15
pip install mxnet-cu102
```

### Download pre-trained models
- Including pre-trained general models and models needed for face reconstruction, identity feature extraction etc
- Download from [BaiduYun](https://pan.baidu.com/s/14-M5TQhNc24275W1ez-gJw)(extract code:usdm) or [GoogleDrive](https://drive.google.com/file/d/17xMNjNEsM0DhS9SKDdUQue0wlbz4Ww9o) and copy to corresponding subfolders (Audio, Deep3DFaceReconstruction, render-to-video).

### Fine-tune on a target peron's short video
- 1. Prepare a talking face video that satisfies: 1) contains a single person, 2) 25 fps, 3) longer than 12 seconds, 4) without large body translation (e.g. move from the left to the right of the screen). An example is [here](Data/31.mp4). Rename the video to [person_id].mp4 (e.g. 1.mp4) and copy to Data subfolder.

Note: You can make a video to 25 fps by 
```bash
ffmpeg -i xxx.mp4 -r 25 xxx1.mp4
```
- 2. Extract frames and landmarks by
```bash
cd Data/
python extract_frame1.py [person_id].mp4
```
- 3. Conduct 3D face reconstruction.
  - First you should compile code in `Deep3DFaceReconstruction/tf_mesh_renderer/mesh_renderer/kernels` to .so. You can call `bazel test` in there.
  - Run:
```bash
cd Deep3DFaceReconstruction/
CUDA_VISIBLE_DEVICES=0 python demo_19news.py ../Data/[person_id]
```
This process takes about 2 minutes on a Titan Xp and 1.6 minutes on a T4.
- 4. Fine-tune the audio network. Run
```bash
cd Audio/code/
python train_19news_1.py [person_id] [gpu_id]
```
The saved models are in `Audio/model/atcnet_pose0_con3/[person_id]`.
This process takes about 5 minutes on a Titan Xp and 6 minutes on a T4.
- 5. Fine-tune the gan network.
Run
```bash
cd render-to-video/
python train_19news_1.py [person_id] [gpu_id]
```
The saved models are in `render-to-video/checkpoints/memory_seq_p2p/[person_id]`.
This process takes about 40 minutes on a Titan Xp and over a hour on a T4.


### Test on a target peron
Place the audio file (.wav or .mp3) for test under `Audio/audio/`.
Run [with generated poses]
```bash
cd Audio/code/
python test_personalized.py [audio] [person_id] [gpu_id]
```
or [with poses from short video]
```bash
cd Audio/code/
python test_personalized2.py [audio] [person_id] [gpu_id]
```
This program will print 'saved to xxx.mov' if the videos are successfully generated.
It will output 2 movs, one is a video with face only (_full9.mov), the other is a video with background (_transbigbg.mov).

## Colab
A colab demo is [here](https://colab.research.google.com/drive/1gqcqTSAGAyj48n0fmApvSPG_43BzKP37).

NOTE: The colab has not been updated.

## Acknowledgments
- The face reconstruction code is from [Deep3DFaceReconstruction](https://github.com/microsoft/Deep3DFaceReconstruction), the arcface code is from [insightface](https://github.com/deepinsight/insightface), the gan code is developed based on [pytorch-CycleGAN-and-pix2pix](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix).
- The 3D face reconstruction uses the [Basel Face Model](https://faces.dmi.unibas.ch/bfm/main.php?nav=1-0&id=basel_face_model).
- The 3D face reconstruction uses expression basis from CoarseData of [Guo et al.](https://github.com/Juyong/3DFace).
